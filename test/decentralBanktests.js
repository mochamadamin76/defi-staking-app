const Tether = artifacts.require('Tether');
const RWD = artifacts.require('RWD');
const DecentralBank = artifacts.require('DecentralBank');

require('chai')
.use(require('chai-as-promised'))
.should()

contract('DecentralBank', ([owner, customer]) => {
    let tether, rwd, decentralBank

    let tokens = (number) => {
        return web3.utils.toWei(number,'ether')
    }

    before(async () => {
        //load Contracts
        tether = await Tether.new()
        rwd = await RWD.new()
        decentralBank = await DecentralBank.new(rwd.address, tether.address)

        //transfer all tokens to DecentralBank (1 Million)
        await rwd.transfer(decentralBank.address, tokens('1000000'))

         //transfer 100 mock Tethers to Customer
        await tether.transfer(customer, tokens('100'), {from: owner})
    })

    describe('Mock Tether Deployment', async  ()=> {
        it('matches name succesfully', async() => {
            const name = await tether.name()
            assert.equal(name,'Mock The Tether')
        })
    })

    describe('Reward Token', async  ()=> {
        it('matches name succesfully', async() => {
            const name = await rwd.name()
            assert.equal(name,'Reward Token')
        })
    })
})